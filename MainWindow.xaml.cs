﻿using System;
using System.Windows;
using System.Windows.Threading;

namespace GaugeDemo
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly DispatcherTimer _circularGaugeTimer;
        private readonly DispatcherTimer _coinFlipTimer;
        private readonly Random _random = new Random();
        private int _evensCount;
        private int _oddsCount;

        public MainWindow()
        {
            InitializeComponent();

            _circularGaugeTimer = new DispatcherTimer {Interval = TimeSpan.FromSeconds(1)};
            _coinFlipTimer = new DispatcherTimer {Interval = TimeSpan.FromMilliseconds(100)};

            _circularGaugeTimer.Tick += (o, args) =>
            {
                var currentValue = ArcScaleComponent1.Needles[0].Value;
                var newValue = currentValue + int.Parse(TextBox1.Text);
                ArcScaleComponent1.Needles[0].Value = newValue;
                CurrentValue.Text = newValue.ToString();
            };

            _coinFlipTimer.Tick += (o, args) =>
            {
                FlipCoin();
            };
        }

        private void FlipCoin()
        {
            var evens = _random.Next(1, 1000) % 2 == 0;

            if (evens)
            {
                _evensCount++;
                LinearGaugeEvensCurrentValue.Text = _evensCount.ToString();
                LinearScaleEvens.Markers[0].Value = _evensCount;
            }
            else
            {
                _oddsCount++;
                LinearGaugeOddsCurrentValue.Text = _oddsCount.ToString();
                LinearScaleOdds.Markers[0].Value = _oddsCount;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _circularGaugeTimer.Start();
            _coinFlipTimer.Start();
        }

        private void ButtonStop_Click(object sender, RoutedEventArgs e)
        {
            _circularGaugeTimer.Stop();
            _coinFlipTimer.Stop();
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            ArcScaleComponent1.Needles[0].Value = 0;
            CurrentValue.Text = 0.ToString();
            LinearScaleEvens.Markers[0].Value = 0;
            LinearScaleOdds.Markers[0].Value = 0;
            LinearGaugeEvensCurrentValue.Text = 0.ToString();
            LinearGaugeOddsCurrentValue.Text = 0.ToString();
        }
    }
}